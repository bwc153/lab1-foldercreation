﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class EditorScript : MonoBehaviour {
    [MenuItem("EditorScript/Create Folders")]

    public static void CreateFolder()
    {
        Debug.Log("Creating a folder(s)!");


        //Dynamic Assets information
        #region Dynamic Assets



        //Create a folder named Dynamic Assets in the Assets folder.
        AssetDatabase.CreateFolder("Assets", "Dynamic Assets");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets", "Resources");


        //All Resources in Dynamic Assets
        #region Dynamic Assets Resources
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animations");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations", "Sources");


        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animation Controllers");
        Debug.Log("Creating Text File");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Effects");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Effects/folderStructure.txt", 
            "The effects folder is where you will place particle effects.  Create a new folder for each particle effect.");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Models");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Models/folderStructure.txt",
            "The effects folder is where you will place particle effects.  Create a new folder for each particle effect.");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Environment");


        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Prefabs");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Prefabs/folderStructure.txt",
            "The Prefabs folder is for all prefabs. A new folder should be created for each level/world/scene the prefab is used in." + 
            "Any prefab used in multiple levels/worlds/scenes should be placed in the common folder. Only one copy of each prefab should exist.");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs", "Common");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Sounds");
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Sounds/folderStructure.txt",
            "The Sounds folder is for all sounds. It should follow the same organization as the prefabs folder.");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");

        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Textures");
        //Create
		System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/Textures/folderStructure.txt",
            "The Textures folder is for all textures that are applied separately to models or changed at run-time.");

        AssetDatabase.CreateFolder("/Dynamic Assets/Resources/Textures", "Common");

        //Create a file named "folderStructure.txt" in the Assets/Dynamic Assets/Resources folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/folderStructure.txt",
            "This folder is for storing materials!");

        AssetDatabase.CreateFolder("Assets", "Editor");
        //Create a file named "folderStructure.txt" in the Assets/Editor folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Editor/folderStructure.txt",
            "The Editor folder has to be created manually." + 
            "This is where you place Editor scripts. The organization of this folder is project-dependent.");

        AssetDatabase.CreateFolder("Assets", "Extensions");
        //Create a file named "folderStructure.txt" in the Assets/Extensions folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Extensions/folderStructure.txt",
            "This is a folder for third party assets such as asset packages." + 
            "The exception to this is the standard assets folder. The standard assets folder should be left in the default location.");

        AssetDatabase.CreateFolder("Assets", "Gizmos");
        //Create a file named "folderStructure.txt" in the Assets/Dynamic Assets/Resources folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Gizmos/folderStructure.txt",
            "This is a folder for gizmo scripts.");

        AssetDatabase.CreateFolder("Assets", "Plugins");
        //Create a file named "folderStructure.txt" in the Assets/Dynamic Assets/Resources folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Plugins/folderStructure.txt",
            "This is a folder for plugin scripts.");

        AssetDatabase.CreateFolder("Assets", "Scripts");
        //Create a file named "folderStructure.txt" in the Assets/Dynamic Assets/Resources folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt",
            "This is a folder for all other scripts." + 
            "It should be separated by common scripts found across multiple objects, and then scripts by level or by type.");

        AssetDatabase.CreateFolder("Assets/Scripts", "Common");

        AssetDatabase.CreateFolder("Assets", "Shaders");
        //Create a file named "folderStructure.txt" in the Assets/Dynamic Assets/Resources folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Shaders/folderStructure.txt",
            "This is a folder for all shader scripts. There is no inherent organizational structure for this folder.");


        #endregion
        #endregion




        //Info for Static Assets
        #region Static Assets

		AssetDatabase.CreateFolder("Assets", "Static Assets");

        //Create a folder named Animations in the Assets folder.
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animations");
		System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Animations/folderStructure.txt", "This folder is for storing Animations!");

        AssetDatabase.CreateFolder("Assets/Static Assets/Animations", "Sources");
        //Create a file named "folderStructure.txt" in the Assets/Animations/Sources folder.
		System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Animations/Sources/folderStructure.txt", "This folder is for storing Animation Sources!");

        //Create a folder named AnimationControllers.
        AssetDatabase.CreateFolder("Assets/Static Assets", "AnimationControllers");
        //Create a file named "folderStructure.txt" in the Assets/AnimationControllers folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/AnimationControllers/folderStructure.txt", "This folder is for storing Animation Controllers!");


        AssetDatabase.CreateFolder("Assets/Static Assets", "Effects");
        //Create a file named "folderStructure.txt" in the Assets/Animations/Sources folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Effects/folderStructure.txt", "This folder is for storing Particle Effects!");

        //Create a Folder named "Models" inside the "Assets" folder.
        AssetDatabase.CreateFolder("Assets/ Static Assets", "Models");
        //Create a file named "folderStructure.txt" in the Assets/Models folder.
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/Models/folderStructure.txt", "This folder is for storing Models!");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Character");
        //Create a file named "folderStructure.txt" in the Assets/Models/Character folder.
        System.IO.File.WriteAllText(Application.dataPath + "Static Assets/Models/Character/folderStructure.txt", "This folder is for storing Characters Models!");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Environment");
        //Create a file named "folderStructure.txt" in the Assets/Models/Environment folder.
        System.IO.File.WriteAllText(Application.dataPath + "Static Assets/Models/Environment/folderStructure.txt", "This folder is for storing Environment Models!");

        //Create a folder named Prefabs in the Assets folder.
        AssetDatabase.CreateFolder("Assets/Static Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs", "Common");

        //Create a folder named Scenes in the Assets folder.
        AssetDatabase.CreateFolder("Assets/Static Assets", "Scenes");

        AssetDatabase.CreateFolder("Assets/Static Assets", "Sounds");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX", "Common");


        //Create a folder named Textures in the assets folder.
        AssetDatabase.CreateFolder("Assets/Static Assets", "Textures");
        AssetDatabase.CreateFolder("Assets/Static Assets/Textures", "Common");




        AssetDatabase.CreateFolder("Assets", "Testing");


        //Refresh the project structure to commit all changes.
        AssetDatabase.Refresh();

        #endregion
    }

}

